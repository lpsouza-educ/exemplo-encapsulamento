namespace Encapsulamento
{
    public class Pessoa
    {
        private int Id;
        private string Nome;
        private int Idade;
        public Pessoa(int id, string nome, int idade)
        {
            this.Id = id;
            this.Nome = nome;
            this.Idade = idade;
        }
        public int ObterId()
        {
            return this.Id;
        }
        public string ObterNome()
        {
            return this.Nome;
        }
        public int ObterIdade()
        {
            return this.Idade;
        }
    }
}
