﻿using System;

namespace Encapsulamento
{
    class Program
    {
        static void Main(string[] args)
        {
            Pessoa[] pessoas = new Pessoa[3];

            pessoas[0] = new Pessoa(1, "Luiz", 38);
            pessoas[1] = new Pessoa(2, "Helena", 32);
            pessoas[2] = new Pessoa(3, "Ana", 60);

            Console.WriteLine("Id\tNome\tIdade");
            for (int i = 0; i < pessoas.Length; i++)
            {
                Console.WriteLine("{0}\t{1}\t{2}",
                    pessoas[i].ObterId(),
                    pessoas[i].ObterNome(),
                    pessoas[i].ObterIdade()
                );
            }
        }
    }
}
